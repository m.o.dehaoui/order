package in.ecommerce.order.util;

public enum OrderStatus {
    CANCELED, ACTIVE
}
