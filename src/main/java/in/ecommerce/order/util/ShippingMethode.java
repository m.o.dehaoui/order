package in.ecommerce.order.util;

public enum ShippingMethode {

    INSTORE_PICKUP, CURBSIDE, SHIP_TO_HOME, THIRD_PARTY
}
