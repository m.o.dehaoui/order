## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What things you need to install 

```
* JDK 8
* Maven
* postgres
* docker

```
### helping softwares 

those are optional you can use any softwares you re familiar with 

```
* vs code 
* postman
* pgadmin

```
### Installing

A step by step series of examples that tell you how to get a development env running
 after instaling the Prerequisites all you need to do is to clone the project then run it 

to add an order you need at least one customer and item in your database 
to build and run docker images execute 
```
*docker-compose up

```
and dont forget to change the datasource url in properties to
```
*spring.datasource.url=jdbc:postgresql://db:5432/ecommerce


```


